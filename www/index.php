<?php

setlocale(LC_CTYPE, 'UTF-8');

// удалить следующую строку в режиме production
defined('YII_DEBUG') or define('YII_DEBUG',true);

$autoload 		= __DIR__.'/../vendor/autoload.php';
$config 		= require_once(__DIR__.'/../src/config/main.php');
$localConfig 	= __DIR__ . '/../src/config/local.php';

if (file_exists($localConfig))
	$config = mergeConfigArray($config, require_once($localConfig));

require_once($autoload);

Yii::setPathOfAlias('debug', __DIR__.'/../vendor/zhuravljov/yii2-debug');

Yii::createWebApplication($config)->run();

/**
 * Рекурсивно мержит массивы с конфигами.
 * Если элемент массива $a - массив, а $b - null, тогда затирает этот элемент в $a
 * array $a массив A
 * array $b массив B
 */
function mergeConfigArray($a, $b) {
	$args=func_get_args();
	$res=array_shift($args);
	while (!empty($args)) {
		$next=array_shift($args);
		foreach ($next as $k => $v) {
			if (is_integer($k))
				isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
			else if (is_null($v) && isset($res[$k]) && is_array($res[$k]))
				unset($res[$k]);
			else if ($v instanceof SReplaceArray)
				$res[$k] = $v->data;
			else if (is_array($v) && isset($res[$k]) && is_array($res[$k]))
				$res[$k] = mergeConfigArray($res[$k],$v);
			else
				$res[$k]=$v;
		}
	}
	return $res;
}