<?php

class m150215_063650_main extends CDbMigration
{

	public function safeUp()
	{
		$this->createTable("user", [
			"`id` int unsigned NOT NULL AUTO_INCREMENT",
			"`name` varchar(255)",
			"`surname` varchar(255)",
			"`login` varchar(255)",
			"`password` varchar(255)",
			'PRIMARY KEY (`id`)'
		],
			'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Пользователи"');

		$this->createTable("post", [
			"`id` int unsigned NOT NULL AUTO_INCREMENT",
			"`text` text",
			"`date` date",
			"`author_id` int unsigned",
			'PRIMARY KEY (`id`)',
			'FOREIGN KEY(`author_id`) REFERENCES `user` (`id`) on delete SET NULL on update cascade'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Записи"');

		$this->createTable('yii_auth_items', [
			'name' => 'varchar(64) not null primary key',
			'type' => 'integer not null',
			'description' => 'text',
			'bizrule' => 'text',
			'data' => 'text',
		], 'Engine=InnoDB DEFAULT CHARSET utf8');

		$this->createTable('yii_auth_item_children', [
			'parent' => 'varchar(64) not null',
			'child' => 'varchar(64) not null',
			'primary key (`parent`,`child`)',
			'foreign key (`parent`) references `yii_auth_items` (`name`) on delete cascade on update cascade',
			'foreign key (`child`) references `yii_auth_items` (`name`) on delete cascade on update cascade',
		], 'Engine=InnoDB DEFAULT CHARSET utf8');

		$this->createTable('yii_auth_assignments', [
			'itemname' => 'varchar(64) not null',
			'userid' => 'integer not null',
			'bizrule' => 'text',
			'data' => 'text',
			'primary key (`itemname`,`userid`)',
			'foreign key (`itemname`) references `yii_auth_items` (`name`) on delete cascade on update cascade',
		], 'Engine=InnoDB DEFAULT CHARSET utf8');

		$rootId = 1;

		$this->execute("INSERT INTO `user` (`id`, `name`, `surname`, `login`, `password`)
						VALUES ({$rootId}, 'root', 'root', 'root', '".CPasswordHelper::hashPassword("root")."')");

		$this->execute("INSERT INTO `yii_auth_items` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
			('admin', 2, 'admin', NULL, NULL),
			('user', 2, 'user', NULL, NULL)");

		$this->execute("INSERT INTO `yii_auth_item_children` (`parent`, `child`) VALUES ('admin', 'user')");

		$this->execute("INSERT INTO `yii_auth_assignments` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', {$rootId}, NULL, 'N;')");

	}

	public function safeDown()
	{
		$this->dropTable("user");
		$this->dropTable("post");
		$this->dropTable("yii_auth_items");
		$this->dropTable("yii_auth_item_children");
		$this->dropTable("yii_auth_assignments");
	}
}