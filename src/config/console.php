<?php

return [

	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'components' => [
		'db' => [
			'connectionString' => 'mysql:host=localhost;dbname=elm',
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		],
	],

	'commandMap' => [
		'migrate' => [
			'class'=>'system.cli.commands.MigrateCommand',
			'migrationPath' => 'application.migrations',
			'migrationTable' => 'migrations',
			'connectionID' => 'db',
		],
	],

];