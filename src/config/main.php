<?php

return [

	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'name'=>'test',
	'language' => 'ru',
	'defaultController' => 'site',

	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>[],

	'components' => [
        'db' => [
			'connectionString' => 'mysql:host=localhost;dbname=elm',
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8'
		],
		'clientScript' => [
			'packages' => [
				'bootstrap' => [
					'basePath' => 'application.vendor.bootstrap',
					'js' => [ 'js/bootstrap.min.js' ],
					'css' => [ 'css/bootstrap.min.css' ],
					'depends' => [ 'jquery' ],
				],
			]
		],
		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
			'assignmentTable' => 'yii_auth_assignments',
			'itemTable' => 'yii_auth_items',
			'itemChildTable' => 'yii_auth_item_children',
		),

		'log' => [
			'class'=>'CLogRouter',
			'routes' => [
				[
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, info',
				],
			],
		],

		'errorHandler' => [
			'errorAction' => 'site/error',
		],
	],

	'params' => [
		'pageSizeUser' => 5,
		'pageSizePost' => 5
	]

];