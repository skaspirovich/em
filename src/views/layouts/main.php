<?php /* @var $this Controller */ ?>
<?
$am = Yii::app()->assetManager;
Yii::app()->clientScript
	->registerCoreScript('jquery')
	->registerPackage('bootstrap')
//	->registerCssFile($am->publish('css/main.css'))
	->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css')
;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8" />
	<title><?= $this->pageTitle ?></title>
	<script>
		var baseUrl = '<?= $this->createAbsoluteUrl('/') ?>';
	</script>
</head>
<body>
<div class="navbar">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?= $this->createUrl('/site/index') ?>">Главная</a>
		</div>
		<div class="collapse navbar-collapse">
			<? $this->widget('zii.widgets.CMenu', [
				'htmlOptions' => [ 'class' => 'nav navbar-nav navbar-right' ],
				'items' => [
					[ 'label' => 'Пользователи', 'url' => [ '/user/admin' ], 'visible' => Yii::app()->user->checkAccess("admin") ],
					[ 'label' => 'Записи', 'url' => [ '/post/admin' ], 'visible' => Yii::app()->user->checkAccess("user") ],
					[ 'label' => 'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible' => !Yii::app()->user->isGuest ],
					[ 'label' => 'Вход', 'url'=>array('/site/login'), 'visible' => Yii::app()->user->isGuest ],
				],
			]); ?>
		</div>
	</div>
</div>
<div class="container" id="container">
	<?= $content ?>
</div>
</body>
</html>