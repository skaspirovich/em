
<h1>Записи</h1>

<p><?= CHtml::link("Новая запись", Yii::app()->createUrl("post/Create")) ?> </p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'post-grid',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'afterAjaxUpdate' => "function(){
			jQuery('#Post_dateFrom').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['ru'],[]));
			jQuery('#Post_dateTo').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['ru'],[]));
		}",
	'columns' => [
		'id',
		'text',
		[
			'name' => 'date',
			'type' => 'raw',
			'value' => 'date("d.m.Y", strtotime($data->date))',
			'filter'=>
				"От:".$this->widget('zii.widgets.jui.CJuiDatePicker', [
					'name' => 'Post[dateFrom]',
					'value' => $model->dateFrom,
					'language' => Yii::app()->language
				], true) .'<br>До:'.
				$this->widget('zii.widgets.jui.CJuiDatePicker', [
					'name' => 'Post[dateTo]',
					'value' => $model->dateTo,
					'language' => Yii::app()->language
				], true)
		],
		[
			'name'=>'user',
			'type'=>'raw',
			'value'=>'CHtml::link($data->user, Yii::app()->createUrl("/user/view", ["id" => $data->id]))'
		],
		[
			'class' => 'CButtonColumn',
			'buttons' => [
				'update' => [
					'visible' => '$data->canShow()'
				],
				'delete' => [
					'visible' => '$data->canShow()'
				]
			],
		]
	]
)); ?>
