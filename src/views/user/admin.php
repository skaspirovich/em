
<h1>Пользователи</h1>

<p><?= CHtml::link("Новый пользователь", Yii::app()->createUrl("user/Create")) ?> </p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'surname',
		'login',
		'password',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
