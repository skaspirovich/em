<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property string $id
 * @property string $text
 * @property string $date
 * @property int $author_id
 * @property string $user
 * @property string $dateFrom
 */
class Post extends CActiveRecord
{
	public $author_id;
	public $dateFrom;
	public $dateTo;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			['text, date', 'required'],
			["author_id", "safe"],
			['id, text, date, user, author_id, dateFrom, dateTo', 'safe', 'on'=>'search'],
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'user' => [self::BELONGS_TO, 'User', 'author_id'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text' => 'Текст',
			'date' => 'Дата',
			'user' => "Автор"
		);
	}

	public function scopes(){
		return [
			'users' => [
				'with' => 'user'
			]
		];
	}

	public function defaultScope()
	{
		return [
			'alias' => 'post'
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$params = [];

		$criteria->scopes = ["users"];

		$criteria->compare('post.id', $this->id);
		$criteria->compare('post.text', $this->text, true);

		if(!empty($this->dateFrom)) {
			$criteria->addCondition("post.date >= :from");
			$params = array_merge($params, [":from" => date("Y-m-d", strtotime($this->dateFrom))]);
		}
		if(!empty($this->dateTo)) {
			$criteria->addCondition("post.date <= :to");
			$params = array_merge($params, [":to" => date("Y-m-d", strtotime($this->dateTo))]);
		}

		if(count($params))
			$criteria->params = array_merge($criteria->params, $params);

		if(!empty($this->user)){
			$criteria->addColumnCondition(['user.name' => $this->user, 'user.surname' => $this->user], "OR");
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>[
				'pageSize'=>Yii::app()->params['pageSizePost'],
				'pageVar'=>'page',
			],
			'sort'=>[
				'defaultOrder'=>'post.id DESC',
			],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Дата в формате d.m.Y
	 * @return bool|null|string
	 */
	public function ruDate(){
		return !empty($this->date) ? date("d.m.Y", strtotime($this->date)) : null;
	}

	public function beforeSave(){
		$this->date = date("Y-m-d", strtotime($this->date));
		$this->author_id = Yii::app()->user->id;
		return parent::beforeSave();
	}

	/**
	 * Проверка на принадлежность записи текущему пользователю
	 * @return bool
	 */
	public function canShow(){
		return $this->author_id == Yii::app()->user->id;
	}

}
