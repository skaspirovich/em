<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $name
 * @property string $surname
 * @property string $login
 * @property string $password
 * @property string $password2
 * @property string $role
 */
class User extends CActiveRecord
{
	public $role;
	public $password2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			['name, surname, login, role', 'required'],
			['name, surname, login, password', 'length', 'max'=>255],
			['login', "unique"],
			['password', 'length', 'min' => 5],
			['password', 'checkPassword'],
			["password, password2", 'required', 'on' => "create"],
			["password", "compare", 'compareAttribute'=>'password2'],
			['id, name, surname, login, password', 'safe', 'on'=>'search'],
			["password, password2", "safe", "on" => "update"]
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'posts' => [self::HAS_MANY, 'Post', 'author_id'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Имя',
			'surname' => 'Фамилия',
			'login' => 'Логин',
			'password' => 'Пароль',
			'password2' => 'Повтор пароля',
			'role' => 'Роль'
		];
	}

	public function checkPassword(){
		if( !empty($this->password) && !(preg_match("/([0-9]+)/", $this->password) &&
			(
				preg_match("/([a-z]+)/", $this->password) ||
				preg_match("/([A-Z]+)/", $this->password) ||
				preg_match("/([а-я]+)/", $this->password) ||
				preg_match("/([А-Я]+)/", $this->password)
			)
		)) {
			$this->addError('password', 'Пароль должен содержать цифры и буквы');
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>[
				'pageSize'=>Yii::app()->params['pageSizeUser'],
				'pageVar'=>'page',
			],
			'sort'=>[
				'defaultOrder'=>'id DESC',
			],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validatePassword($password)
	{
		return CPasswordHelper::verifyPassword($password,$this->password);
	}

	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}

	public function beforeSave(){
		switch($this->getScenario()){
			case "create":
				$this->password = $this->hashPassword($this->password);
				break;
			case "update":
				$oldPassHash = User::model()->findByPk($this->id)->password;
				if(empty($this->password)){
					$this->password = $oldPassHash;
				}else{
					$this->password = $this->hashPassword($this->password);
				}
				break;
			default:
				throw new CException('Указан неизвестный сценарий');
				break;
		}
		return parent::beforeSave();
	}

	public function afterSave(){

		$assignments = Yii::app()->authManager->getAuthAssignments($this->id);
		if (!empty($assignments)) {
			foreach ($assignments as $key => $assignment) {
				Yii::app()->authManager->revoke($key, $this->id);
			}
		}

		Yii::app()->authManager->assign($this->role, $this->id);

		return parent::afterSave();
	}

	public function __toString(){
		return $this->surname." ".$this->name;
	}

}
